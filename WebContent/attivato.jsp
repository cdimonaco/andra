<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<title>A.N.D.R.A. Modalità Attivato</title>
</head>
<body>
<c:if test="${not empty sessionScope.error }">
<div class="alert alert-danger">
  <strong>ERRORE!</strong> ${sessionScope.error}
</div>
</c:if>
<c:if test="${not empty sessionScope.message }">
<div class="alert alert-success">
  <strong>SUCCESSO!</strong> ${sessionScope.message}
</div>
</c:if>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="page-header">
				<h1>MODALITA' ATTIVATO</h1>
			</div>
			<a href="${pageContext.request.contextPath}/collaudo" ><button type="button" style="margin-left:20px;margin-bottom:15px" class="btn btn-primary text-left">Modalità Collaudo</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID Sensore</th>
					<th>Nome Sensore</th>
					<th>MinValue</th>
					<th>MaxValue</th>
					<th>CurrValue</th>
					<th>Status: 0 Disattivato/1 Attivato</th>					
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${sensors}" var="sensor">
   				<tr>
			       <td>${sensor.getId()}</td>
			       <td>${sensor.getName()}</td>
			       <td>${sensor.getMinValue()}</td>
				   <td>${sensor.getMaxValue()}</td>
				   <td>${sensor.getCurrValue()}</td>
				   <td>${sensor.getActivated()}</td>
    			</tr>
			</c:forEach>
			</tbody>
			</table>
		</div>
	</div>
</div>
<c:if test="${not empty alarm }">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">Allarme In corso</div>
  				<div class="panel-body">
  				<p>Nome Allarme: ${alarm.getName()}"</p>
  				<p>Sensore coinvolto:${alarm.getSensor()}</p>
  				<p>Date e ora:${alarm.getTimestamp().toString()}</p>
  				<p>Sensore intervento azionato: ${action.getName()}</p>
  				<form action="alarm" method="POST">
  					<input type="hidden" name="_method" value="DELETE">
  					 <button type="submit" class="btn btn-danger">Ferma Allarme</button>
  				</form>
  				</div>
			</div>
		</div>
	</div>
</c:if>

</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>