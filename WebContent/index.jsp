<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>A.N.D.R.A - Gestione Domotica - Home</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="page-header">
				<h1>A.N.D.R.A - Gestione Domotica</h1>
			</div>
			
		</div>
	</div>
	<div class="row" style="margin-bottom:50px">
		<div class="col-lg-4" style="margin-left:100px">
			<img height="200" width="200" class="img-responsive" src="http://www.uniparthenope.it/images/loghi/Logo_g.jpg">
		</div>
		<div class="col-lg-6 text-center" style="margin-left:40px">
			<h4>Progetto per l'esame di Programmazione III</h4>
			<h4>Studente: Di Monaco Carmine - 0124001236</h4>
			<h4>Cdl in Informatica </h4>
			<h4>A.A. 2016/2017 </h4>
		</div>
	</div>
	<div class="row" style="margin-bottom:40px">
		<div class="col-lg-12 text-center">
			<a href="${pageContext.request.contextPath}/sensors" ><button style="margin-bottom:15px" type="button" class="btn btn-primary btn-block">Modalità Attivato</button></a>
			<a href="${pageContext.request.contextPath}/collaudo" ><button type="button" class="btn btn-primary btn-block">Modalità Collaudo</button></a>
		</div>
	</div>
	<div class="row">
	<div class="col-lg-12 text-center">
		<h3>MADE WITH</h3><br>
	</div>
		<div class="col-lg-6">
			<img height="150" width="150" class="img-responsive center-block" src="http://tarnaeluin.houseofbeor.net/wp-content/uploads/2014/08/Java-Logo.jpg">
		</div>
		<div class="col-lg-6">
			<img height="150" width="150" class="img-responsive center-block" src="https://www.nucleus.be/public/images/assets/mysql_logo.png">
		</div>
	</div>
</div>
</body>
</html>