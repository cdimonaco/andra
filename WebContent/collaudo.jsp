<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script type="text/javascript">
function validateAddSensorForm() {
    var sensorname = document.forms["sensorinsertform"]["sensorname"].value;
    var minvalue = document.forms["sensorinsertform"]["minvalue"].value;
    var maxvalue = document.forms["sensorinsertform"]["maxvalue"].value;


    if (sensorname == "" || minvalue == "" || maxvalue == "") {
        alert("Controlla il form");
        return false;
    }
}

function validateSensorActivationForm() {
    var sensorid = document.forms["sensoractivationform"]["sensorid"].value;
    var sensorstatus = document.forms["sensoractivationform"]["sensorstatus"].value;
    if (sensorid == "-1" || sensorstatus == "-1") {
        alert("Controlla il form");
        return false;
    }
}
function validateActionInsertForm() {
    var actionname = document.forms["actioninsertform"]["actionname"].value;
    var actionsensor = document.forms["actioninsertform"]["actionsensor"].value;
    if (actionname == "" || actionsensor == "-1") {
        alert("Controlla il form");
        return false;
    }
}

function validateModuleInsertForm() {
    var modulename = document.forms["moduleinsertform"]["modulename"].value;
    var description = document.forms["moduleinsertform"]["description"].value;
    if (modulename == "" || description == "") {
        alert("Controlla il form");
        return false;
    }
}
function validateModuleAttachForm(){
    var sensorid = document.forms["moduleattachform"]["sensorid"].value;
    var moduleid = document.forms["moduleattachform"]["moduleid"].value;
    if (sensorid == "-1" || moduleid == "-1") {
        alert("Controlla il form");
        return false;
    }
}
function validateAlarmInsertForm(){
    var sensorname = document.forms["alarminsertform"]["alarmname"].value;
    var sensorid = document.forms["alarminsertform"]["alarmsensor"].value;
    if (sensorname == "" || sensorid == "-1") {
        alert("Controlla il form");
        return false;
    }
}

</script>
<title>A.N.D.R.A. Modalità Collaudo</title>
</head>
<body>
<c:if test="${not empty sessionScope.error }">
<div class="alert alert-danger">
  <strong>ERRORE!</strong> ${sessionScope.error}
</div>
</c:if>
<c:if test="${not empty sessionScope.message }">
<div class="alert alert-success">
  <strong>SUCCESSO!</strong> ${sessionScope.message}
</div>
</c:if>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="page-header">
				<h1>MODALITA' COLLAUDO</h1>
			</div>
			<a href="${pageContext.request.contextPath}/sensors" ><button type="button" style="margin-left:20px;margin-bottom:15px" class="btn btn-primary text-left">Modalità Attivato</button></a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<form action="sensors" method="POST" name="sensorinsertform" onsubmit="return(validateAddSensorForm());">
				  <div class="form-group">
				    <label for="sensorname">Nome Sensore</label>
				    <input type="text" class="form-control" id="sensorname" name="sensorname">
				  </div>
				  <div class="form-group">
				    <label for="minvalue">Valore minimo</label>
				    <input type="text" class="form-control" id="minvalue" name="minvalue">
				  </div>
				  <div class="form-group">
				    <label for="maxvalue">Valore soglia</label>
				    <input type="text" class="form-control" id="maxvalue" name="maxvalue">
				  </div>
				  <input type="hidden" name="_method" value="POST">
				  <button type="submit" class="btn btn-default">Crea Sensore</button>
			</form>
		</div>
		<div class="col-lg-6">
			<form action="sensors" method="POST" name="sensoractivationform" onsubmit="return(validateSensorActivationForm());">
				  <div class="form-group">
				    <label for="sensorid">Sensore</label>
				    <select class="form-control" id="sensorid" name="sensorid">
				    	<option value="-1">Seleziona sensore</option>
				    	<c:forEach items="${sensors}" var="sensor">
				    		<option value="${sensor.getId() }">${sensor.getName() }</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <div class="form-group">
				    <label for="sensorstatus">Stato</label>
				    <select class="form-control" id="sensorstatus" name="sensorstatus">
				    	<option value ="-1">Seleziona status</option>
				    	<option value ="0">Disattivato</option>
				    	<option value ="1">Attivato</option>
				    </select>
				  </div>
				  <input type="hidden" value="UPDATE" name="_method">
				  <button type="submit" class="btn btn-default">Cambia stato sensore</button>
			</form>
		</div>
	</div>
<div class="row">
		<div class="col-lg-6">
			<form action="modules" method="POST" name="moduleinsertform" onsubmit="return(validateModuleInsertForm());">
				  <div class="form-group">
				    <label for="modulename">Nome Modulo</label>
				    <input type="text" class="form-control" id="modulename" name="modulename">
				  </div>
				  <div class="form-group">
				    <label for="description">Descrizione</label>
				    <input type="text" class="form-control" id="description" name="description">
				  </div>
				  <input type="hidden" name="_method" value="POST">
				  <button type="submit" class="btn btn-default">Crea Modulo</button>
			</form>
		</div>
		<div class="col-lg-6">
			<form action="modules" method="POST" name="moduleattachform" onsubmit="return(validateModuleAttachForm());">
				  <div class="form-group">
				    <label for="sensorid">Sensore</label>
				    <select class="form-control" id="sensorid" name="sensorid">
				    	<option value="-1">Seleziona sensore</option>
				    	<c:forEach items="${sensors}" var="sensor">
				    		<option value="${sensor.getId() }">${sensor.getName() }</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <div class="form-group">
				    <label for="moduleid">Modulo</label>
				    <select class="form-control" id="moduleid" name="moduleid">
				    	<option value ="-1">Seleziona modulo</option>
				    	<c:forEach items="${modules}" var="module">
				    		<option value="${module.getId() }">${module.getName() }</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <input type="hidden" value="PUT" name="_method">
				  <button type="submit" class="btn btn-default">Aggancia modulo</button>
			</form>
		</div>
</div>
<div class="row">
	<div class="col-lg-6">
			<form action="sensors" method="POST" name="actioninsertform" onsubmit="return(validateActionInsertForm());">
				  <div class="form-group">
				    <label for="actionname">Nome Sensore Intervento</label>
				    <input type="text" class="form-control" id="actionname" name="actionname">
				  </div>
				  <div class="form-group">
				    <label for="actionsensor">Sensore monitoraggio associato</label>
				    <select class="form-control" id="actionsensor" name="actionsensor">
				    	<option value="-1">Seleziona sensore</option>
				    	<c:forEach items="${sensors}" var="sensor">
				    		<option value="${sensor.getId() }">${sensor.getName()}</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <input type="hidden" name="_method" value="ALARM_HANDLER">
				  <button type="submit" class="btn btn-default">Crea Sensore Intervento</button>
			</form>
		</div>
		<div class="col-lg-6">
			<form action="alarm" method="POST" name="alarminsertform" onsubmit="return(validateAlarmInsertForm());">
				  <div class="form-group">
				    <label for="alarmname">Nome Allarme</label>
				    <input type="text" class="form-control" id="alarmname" name="alarmname">
				  </div>
				  <div class="form-group">
				    <label for="alarmsensor">Sensore monitoraggio coinvolto</label>
				    <select class="form-control" id="alarmsensor" name="alarmsensor">
				    	<option value="-1">Seleziona sensore</option>
				    	<c:forEach items="${sensors}" var="sensor">
				    		<option value="${sensor.getId() }">${sensor.getName()}</option>
				    	</c:forEach>
				    </select>
				  </div>
				  <input type="hidden" name="_method" value="">
				  <button type="submit" class="btn btn-default">Crea Allarme</button>
			</form>
		</div>
</div>
<div class="row">
	<div class="col-lg-12 text-center" style="margin-top:20px">
		<form action="sensors" method="POST">
			<input type="hidden" name="_method" value="DELETE">
			<button type="submit" class="btn btn-danger">RESET SENSORI</button>
		</form>
	</div>
</div>
<div class="row">
		<div class="col-lg-12">
			<div class="page-header">
				<h3>STATISTICHE</h3>
			</div>
		</div>
		<div class="col-lg-12">
			<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID Sensore Coinvolto</th>
					<th>Nome Allarme</th>
					<th>Timestamp</th>
					<th>Status - 1 Attivo / 0 Disattivato</th>				
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${alarms}" var="alarm">
   				<tr>
			       <td>${alarm.getSensor()}</td>
			       <td>${alarm.getName()}</td>
			       <td>${alarm.getTimestamp().toString()}</td>
				   <td>${alarm.getActivated()}</td>
    			</tr>
			</c:forEach>
			</tbody>
			</table>
		</div>
	<div class="col-lg-12">
			<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id modulo</th>
					<th>Nome modulo</th>
					<th>Descrizione</th>				
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${modules}" var="module">
   				<tr>
			       <td>${module.getId()}</td>
			       <td>${module.getName()}</td>
			       <td>${module.getDescription()}</td>
    			</tr>
			</c:forEach>
			</tbody>
			</table>
		</div>
		<div class="col-lg-12">
			<table class="table table-bordered">
			<thead>
				<tr>
					<th>Id Sensore Intervento</th>
					<th>Nome</th>
					<th>Sensore monitoraggio associato</th>				
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${actions}" var="action">
   				<tr>
			       <td>${action.getId()}</td>
			       <td>${action.getName()}</td>
			       <td>${action.getSensor()}</td>
    			</tr>
			</c:forEach>
			</tbody>
			</table>
		</div>
	</div>
	</div>
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>