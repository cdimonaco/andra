/**
 * 
 */
package util;

/**
 * @author cdimonaco
 * Classe singleton per instanziare una connessione al database.
 * L'utilizzo del pattern singleton mi permette una migliore ottimizzazione di risorse e memoria.
 */
import java.sql.*;
import java.util.logging.*;

public class DatabaseUtility {

    private static DatabaseUtility dbIsntance;
    public static final String URL = "jdbc:mysql://localhost";
    public static final String USER = "root";
    public static final String PASSWORD = "carmine";
    public static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";

    private static Connection con;

    private DatabaseUtility() {
    }

    public static synchronized DatabaseUtility getInstance() {
        if (dbIsntance == null) {
            dbIsntance = new DatabaseUtility();
        }
        return dbIsntance;
    }

    public Connection getConnection() {

        if (con == null) {
            try {
                Class.forName(DRIVER_CLASS);
                con = DriverManager.getConnection(URL + "/domotica" +"?&serverTimezone=UTC", USER, PASSWORD);
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(DatabaseUtility.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return con;
    }
}
