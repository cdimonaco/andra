/**
 * 
 */
package dao.sensor;

import java.util.ArrayList;

import java.sql.*;
import models.*;
import util.DatabaseUtility;
/**
 * @author cdimonaco
 * 
 * Classe SensorsDAO, implementazione del Dao per i sensori monitoraggio.
 * Nei vari metodi, gestisco le operazioni CRUD sul database.
 * Utilizzo gli oggetti messi a disposizione dal package java.sql per interagire con la base di dati.
 */

public class SensorsDAO implements SensorsHandler {
	private DatabaseUtility db;
	public SensorsDAO(DatabaseUtility dbinstance){
		this.db = dbinstance;
	}
	
	/**
	 * Questo metodo inserisce un nuovo sensore nel database, dopo aver fatto un prepared statement,
	 * con le informazioni prese dal model passato dalla servlet. Di default tutti i sensori sono attivati
	 * al momento della loro creazione.
	 *
	 */
	@Override
	public boolean Insert(SensorModel model) {
		Connection connection = db.getConnection();
		String QUERY = "INSERT INTO sensor (Name,minvalue,topvalue,activated) VALUES(?,?,?,?)";
		try{
		PreparedStatement pstm = connection.prepareStatement(QUERY);
		pstm.setString(1,model.getName());
		pstm.setFloat(2,model.getMinValue());
		pstm.setFloat(3,model.getMaxValue());
		pstm.setInt(4, 1);
		pstm.executeUpdate();
		}catch(SQLException e){
			System.out.println("FAILED"+e.getMessage());
			return false;
			
		}
		System.out.println("SUCCESS");
		return true;
	}
	
	@Override
	public boolean resetAll(){
		Connection connection = db.getConnection();
		String QUERY = "UPDATE sensor SET currvalue=0 where idsensor =  any(SELECT idsensor from (select idsensor from sensor) as c)";
		try{
			Statement statement = connection.createStatement();
			statement.executeUpdate(QUERY);
		}catch(SQLException e){
			System.out.println(e.toString());
			return false;
		}
		return true;
	}
	
	@Override
	public ArrayList<SensorModel> getAll(){
		ArrayList<SensorModel> allSensors = new ArrayList<SensorModel>();
		Connection connection = db.getConnection();
		String QUERY = "SELECT * FROM sensor";
		try{
		Statement statement = connection.createStatement();
		ResultSet results = statement.executeQuery(QUERY);
		while(results.next()){
			allSensors.add(new SensorModel(results.getInt(1),results.getString(2),results.getFloat(3),results.getFloat(4),results.getFloat(5),results.getInt(6)));
		}
		}catch(SQLException e){
			System.out.println("FAILED");
		}
	
		return allSensors;
	}

	@Override
	public boolean activationSensor(int idsensor,int action) {
		Connection connection = db.getConnection();
		String QUERY = "UPDATE domotica.sensor set activated = ? where idsensor=?";
		try{
		PreparedStatement pstm = connection.prepareStatement(QUERY);
		pstm.setInt(1, action);
		pstm.setInt(2, idsensor);
		pstm.executeUpdate();
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return false;
		}
		return true;
	}
	
}
