package dao.sensor;

import java.util.ArrayList;

import models.SensorModel;

public interface SensorsHandler {
	public boolean Insert(SensorModel model);
	public boolean resetAll();
	public ArrayList<SensorModel> getAll();
	public boolean activationSensor(int idsensor,int action);
}
