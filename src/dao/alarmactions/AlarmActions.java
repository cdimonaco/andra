package dao.alarmactions;

import java.util.ArrayList;

import models.ActionModel;

public interface AlarmActions {
	boolean insert(ActionModel Model);
	ActionModel toCurrentAlarm();
	ArrayList<ActionModel> getAll();
	boolean attach(int idsensor,int actionid);
}
