package dao.alarmactions;

import java.util.ArrayList;

import java.sql.*;

import models.ActionModel;
import util.DatabaseUtility;
/**
 * Classe AlarmHandlerDAO, implementazione del Dao per i sensori intervento.
 * Nei vari metodi, gestisco le operazioni CRUD sul database.
 * Utilizzo gli oggetti messi a disposizione dal package java.sql per interagire con la base di dati.
 */
public class AlarmHandlerDAO implements AlarmActions {
	private DatabaseUtility db;
	
	public AlarmHandlerDAO(DatabaseUtility db){
		this.db=db;
	}
	
	@Override
	public boolean insert(ActionModel Model) {
		Connection connection = db.getConnection();
		String QUERY = "INSERT INTO alarm_handler (Name,sensor,activated) VALUES (?,?,?)";
		try{
			PreparedStatement pstm = connection.prepareStatement(QUERY);
			pstm.setString(1, Model.getName());
			pstm.setInt(2,Model.getSensor());
			pstm.setInt(3, 0);
			pstm.executeUpdate();
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return false;
		}
		return true;
	}

	@Override
	public ActionModel toCurrentAlarm() {
		Connection connection = db.getConnection();
		String QUERY = "SELECT * from alarm_handler where sensor = ( SELECT sensor from (select sensor from alarm where activated = 1 order by time ASC LIMIT 1) as c)";
		try{
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery(QUERY);
			if (!results.next()){
				System.out.println("NO ALARM HANDLER FOR SELECTED ALARM");
				return null;
			}
			return new ActionModel(results.getString(2),results.getInt(1),results.getInt(3),results.getInt(4));
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return null;
		}
	}

	@Override
	public ArrayList<ActionModel> getAll() {
		ArrayList<ActionModel> allActions = new ArrayList<ActionModel>();
		Connection connection = db.getConnection();
		String QUERY = "SELECT * FROM alarm_handler";
		try{
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery(QUERY);
			while(results.next()){
				allActions.add(new ActionModel(results.getString(2),results.getInt(1),results.getInt(3),results.getInt(4)));
			}
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return null;
		}
		return allActions;
	}

	@Override
	public boolean attach(int idsensor, int actionid) {
		Connection connection = db.getConnection();
		String QUERY = "UPDATE alarm_handler SET sensor = ? where idalarm_handler =  ?";
		try{
			PreparedStatement pstm = connection.prepareStatement(QUERY);
			pstm.setInt(1, idsensor);
			pstm.setInt(2, actionid);
			pstm.executeUpdate();
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return false;
		}
		return true;
	}

}
