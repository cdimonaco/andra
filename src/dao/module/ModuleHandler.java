package dao.module;

import java.util.ArrayList;

import models.ModuleModel;

public interface ModuleHandler {
	public boolean Insert(ModuleModel model);
	public boolean Attach(int moduleid, int sensorid);
	public ArrayList<ModuleModel> getAll();
}
