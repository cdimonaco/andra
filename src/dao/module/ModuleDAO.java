package dao.module;

import java.util.ArrayList;

import java.sql.*;

import models.ModuleModel;
import util.DatabaseUtility;

/**
 * Classe ModuleDAO, implementazione del Dao per i moduli dei sensori.
 * Nei vari metodi, gestisco le operazioni CRUD sul database.
 * Utilizzo gli oggetti messi a disposizione dal package java.sql per interagire con la base di dati.
 */
public class ModuleDAO implements ModuleHandler{
	
	private DatabaseUtility db;
	
	public ModuleDAO(DatabaseUtility database){
		db=database;
	}
	
	@Override
	public boolean Insert(ModuleModel model) {
		Connection connection = db.getConnection();
		String QUERY = "INSERT INTO modules (name,description) VALUES (?,?)";
		try{
			PreparedStatement pstm = connection.prepareStatement(QUERY);
			pstm.setString(1,model.getName());
			pstm.setString(2,model.getDescription());
			pstm.executeUpdate();
			}catch(SQLException e){
				System.out.println("FAILED"+e.getMessage());
				return false;
				
			}
		return true;
	}

	@Override
	public boolean Attach(int moduleid, int sensorid) {
		Connection connection = db.getConnection();
		String QUERY = "INSERT INTO attached_m (sensorid,moduleid) VALUES (?,?)";
		try{
			PreparedStatement pstm = connection.prepareStatement(QUERY);
			pstm.setInt(1,sensorid);
			pstm.setInt(2,moduleid);
			pstm.executeUpdate();
		}catch(SQLException e){
			System.out.println("FAILED"+e.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public ArrayList<ModuleModel> getAll() {
		ArrayList<ModuleModel> allModules = new ArrayList<ModuleModel>();
		Connection connection = db.getConnection();
		String QUERY = "SELECT * FROM modules";
		try{
			Statement statement = connection.createStatement();
			ResultSet results = statement.executeQuery(QUERY);
			while(results.next()){
				allModules.add(new ModuleModel(results.getInt(1),results.getString(2),results.getString(3)));
			}
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return null;
		}
		return allModules;
	}
	
}
