/**
 * 
 */
package dao;
import dao.alarm.AlarmHandler;
import dao.module.ModuleHandler;
import dao.sensor.SensorsHandler;
import dao.alarmactions.AlarmActions;

/**
 * @author cdimonaco
 *	Classe astratta per la factory dei Dao.
 */
public abstract class DAOFactory {
	public abstract AlarmActions getAlarmsActionsDAO();
	public abstract SensorsHandler getSensorsDAO();
	public abstract ModuleHandler getModuleDAO();
	public abstract AlarmHandler getAlarmDAO();
}