package dao;

import dao.alarm.AlarmDAO;
import dao.alarm.AlarmHandler;
import dao.alarmactions.*;
import dao.alarmactions.AlarmActions;
import dao.module.ModuleDAO;
import dao.module.ModuleHandler;
import dao.sensor.SensorsDAO;
import dao.sensor.SensorsHandler;
import util.DatabaseUtility;

/**
 * Implementazione concreta del factory method per la creazione di oggetti DAO
 * Usando il pattern Dao (Data access object), attuo una netta separazione tra la business logic
 * e la gestione della persistenza, in questo caso data dal database MySql.
 * Espongo quindi solo l'interfaccia con cui si effettueranno le operazioni sui dati.
 * Creo un oggetto Dao per ogni model della mia web application, sostanzialmente per ogni tabella del mio database.
 * Per ottimizzare la gestione e l'utilizzo delle risorse applico il pattern singleton alla factory.
 * Nel costruttore andrà ad ottenere la connessione al database dalla classe "DatabaseUtility", passandola come parametro
 * ai vari Dao.
 * 
 */
public class MysqlDAOFactory extends DAOFactory {
	private DatabaseUtility dbinstance;
	
	private static MysqlDAOFactory instance;
	
	public synchronized static MysqlDAOFactory getInstance() {
        if (instance == null) {
            instance = new MysqlDAOFactory();
        }
        return instance;
    }
	
	private MysqlDAOFactory(){
		this.dbinstance = DatabaseUtility.getInstance();
	}
	
	@Override
	public AlarmActions getAlarmsActionsDAO() {
		// TODO Auto-generated method stub
		return new AlarmHandlerDAO(dbinstance);
	}

	@Override
	public SensorsHandler getSensorsDAO() {
		// TODO Auto-generated method stub
		return new SensorsDAO(dbinstance);
	}

	@Override
	public ModuleHandler getModuleDAO() {
		// TODO Auto-generated method stub
		return new ModuleDAO(dbinstance);
	}

	@Override
	public AlarmHandler getAlarmDAO() {
		// TODO Auto-generated method stub
		return new AlarmDAO(dbinstance);
	}

}
