package dao.alarm;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import models.AlarmModel;
import util.DatabaseUtility;

/**
 * Classe AlarmDao, implementazione del Dao per gli allarmi.
 * Nei vari metodi, gestisco le operazioni CRUD sul database.
 * Utilizzo gli oggetti messi a disposizione dal package java.sql per interagire con la base di dati.
 */
public class AlarmDAO implements AlarmHandler{
	
	private DatabaseUtility dbinstance;
	
	public AlarmDAO(DatabaseUtility database){
		dbinstance = database;
	}
	
	@Override
	/**
	 * Tale metodo, si occupa di simulare l'attivazione di un allarme, esso infatti creerà un allarme e attiverà il sensore
	 * intervento associato al sensore monitoraggio coinvolto.
	 */
	public boolean Insert(AlarmModel Model) {
		Connection connection = dbinstance.getConnection();
		String QUERY = "INSERT INTO alarm (Name,sensor,activated) VALUES(?,?,?)";
		String QUERY2 = "UPDATE alarm_handler set activated = 1 where sensor = ( SELECT sensor from (select sensor from alarm where activated = 1 order by time ASC LIMIT 1) as c)";
		try{
			Statement statement = connection.createStatement();
			PreparedStatement pstm = connection.prepareStatement(QUERY);
			pstm.setString(1, Model.getName());
			pstm.setInt(2,Model.getSensor());
			pstm.setInt(3, 1);
			pstm.executeUpdate();
			statement.executeUpdate(QUERY2);
		}catch(SQLException e){
			System.out.println("FAILED"+e.toString());
			return false;
		}
		return true;
	}

	@Override
	/**
	 * In questo metodo, elimino un allarme, faccio uso della medesima strategia di getAlarm(),
	 * disattivo inoltre il sensore intervento associato al sensore che ha generato l'allarme.
	 */
	public boolean Delete() {
		Connection connection = dbinstance.getConnection();
		String QUERY2 = "UPDATE alarm_handler set activated = 0 where sensor = ( SELECT sensor from (select sensor from alarm where activated = 1 order by time ASC LIMIT 1) as c)"
;		String QUERY = "UPDATE alarm set activated = 0 where idalarm = ( SELECT idalarm from (select idalarm from alarm where activated = 1 order by time ASC LIMIT 1) as c)";
		try{
			Statement statement = connection.createStatement();
			statement.executeUpdate(QUERY2);
			statement.executeUpdate(QUERY);
		}catch(SQLException e){
			System.out.print("FAILED"+e.toString());
			return false;
		}
		return true;
	}
	/**
	 * Questo metodo, racchiude tutta la gestione FIFO degli allarmi,
	 * Ho deciso, per separare ancora di più la business logic dalla gestione dei dati, di non usare nessuna struttura
	 * dati o logica offerta dal linguaggio Java, ma di gestire la politica degli allarmi sul database.
	 * Infatti è la query in se che fornisce la sicurezza della politica FIFO, poichè ritornerà un solo allarme attivato
	 * ordinato in modo "ASC",ovvero il primo allarme scattato in ordine di tempo. Quindi FIFO (First in,first out).
	 */
	@Override
	public AlarmModel getAlarm() throws NoAlarmException{
		Connection connection = dbinstance.getConnection();
		String QUERY = "SELECT * from alarm where activated = 1 order by time ASC LIMIT 1";
		try{
		Statement statement = connection.createStatement();
		ResultSet results = statement.executeQuery(QUERY);
		if (!results.next()){
			throw new NoAlarmException("Nessun allarme");
			
		}
		return new AlarmModel(results.getString(2),results.getTime(5),results.getInt(3),results.getInt(4));
		}catch(SQLException e){
			System.out.println("FAILED"+e.getMessage());
		}
		return null;
	}

	@Override
	public ArrayList<AlarmModel> getAll() {
		ArrayList<AlarmModel> allAlarms = new ArrayList<AlarmModel>();
		Connection connection = dbinstance.getConnection();
		String QUERY = "SELECT * from alarm";
		try{
		Statement statement = connection.createStatement();
		ResultSet results = statement.executeQuery(QUERY);
		while(results.next()){
			allAlarms.add(new AlarmModel(results.getString(2),results.getTime(5),results.getInt(3),results.getInt(4)));
		}
		}catch(SQLException e){
			System.out.println(e.toString());
			return null;
		}
		return allAlarms;
	}
	
}