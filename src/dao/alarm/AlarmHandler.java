package dao.alarm;

import java.util.ArrayList;

import models.AlarmModel;

public interface AlarmHandler {
	public boolean Insert(AlarmModel Model);
	public boolean Delete();
	public AlarmModel getAlarm() throws NoAlarmException;
	public ArrayList<AlarmModel> getAll();
}
