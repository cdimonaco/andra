package dao.alarm;

@SuppressWarnings("serial")
public class NoAlarmException extends Exception {
	public NoAlarmException(String message){
		super(message);
	}
}
