package servlets;

import java.io.IOException;
import java.util.ArrayList;

import dao.MysqlDAOFactory;
import dao.alarm.AlarmHandler;
import dao.sensor.SensorsHandler;
import models.*;
import dao.alarmactions.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SensorsServlet
 * Tramite il method HTTP "POST", indentifico il tipo di azione che deve compiere la servlet, utilizzo un campo nel form
	 * con attributo "hidden", contenente l'azione.
	 * Sarà quindi compito preliminare della servlet, leggere questo parametro dalla richiesta per richiamare il metodo corretto.
	 * Va fatto menzione dell'utilizzo delle Sessioni, necessarie per il trasferimento dei messaggi flash al frontend, per evitare il sovrapporsi
	 * di messaggi e/o di errori, la sessione viene invalidata dopo la trasmissione dei messsaggi e/o errori.
 */
@WebServlet(urlPatterns="/sensors",name="SensorServlet")
public class SensorsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SensorsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SensorsHandler SensorDao = MysqlDAOFactory.getInstance().getSensorsDAO();
		AlarmHandler AlarmDao = MysqlDAOFactory.getInstance().getAlarmDAO();
		AlarmActions AlarmHandlerDao = MysqlDAOFactory.getInstance().getAlarmsActionsDAO();
		ArrayList<SensorModel> allSensors = SensorDao.getAll();
		try{
		AlarmModel alarm = AlarmDao.getAlarm();
		ActionModel action = AlarmHandlerDao.toCurrentAlarm();
		request.setAttribute("alarm", alarm);
		request.setAttribute("action", action);
		}catch(Exception e){
			//No alarm passed to jsp
		}
        request.setAttribute("sensors", allSensors);
        request.getRequestDispatcher("/attivato.jsp").forward(request, response);
        request.getSession().invalidate();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("_method");
		if(method.equalsIgnoreCase("DELETE")){
			doDelete(request,response);
			return;
		}else if(method.equalsIgnoreCase("UPDATE")){
			doActivation(request,response);
			return;
		}else if(method.equalsIgnoreCase("ALARM_HANDLER")){
			doAlarmHandler(request,response);
			return;
		}
		String sensorName = request.getParameter("sensorname");
		float minValue = Float.parseFloat(request.getParameter("minvalue"));
		float maxValue = Float.parseFloat(request.getParameter("maxvalue"));
		SensorsHandler SensorDao = MysqlDAOFactory.getInstance().getSensorsDAO();
		SensorModel newSensor = new SensorModel(0,sensorName,maxValue,minValue,0,1);
		boolean status = SensorDao.Insert(newSensor);
		if(!status){
			request.getSession().setAttribute("error", "Errore nella creazione del sensore di monitoraggio\n");
		}else{
			request.getSession().setAttribute("message", "Sensore di monitoraggio creato con successo!\n");

		}
		response.sendRedirect("collaudo");;
	}
	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		SensorsHandler SensorDao = MysqlDAOFactory.getInstance().getSensorsDAO();
		SensorDao.resetAll();
		request.getSession().setAttribute("message", "Reset Completato con successo\n");
		response.sendRedirect("collaudo");;
	}
	
	protected void doAlarmHandler(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		AlarmActions alarmHandlerDao = MysqlDAOFactory.getInstance().getAlarmsActionsDAO();
		String actionname = request.getParameter("actionname");
		int actionsensor = Integer.parseInt(request.getParameter("actionsensor"));
		boolean status = alarmHandlerDao.insert(new ActionModel(actionname,0,actionsensor,0));
		if(!status){
			request.getSession().setAttribute("error", "Errore nella creazione del sensore di intervento,probabilmente stai cercando di assegnarlo ad un sensore monitoraggio che ne ha già uno.\n");
		}else{
			request.getSession().setAttribute("message", "Sensore di intervento creato con successo!\n");
		}
		response.sendRedirect("collaudo");
	}

	protected void doActivation(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		int sensorid = Integer.parseInt(request.getParameter("sensorid"));
		int sensorstatus = Integer.parseInt(request.getParameter("sensorstatus"));

		SensorsHandler SensorDao = MysqlDAOFactory.getInstance().getSensorsDAO();
		boolean status = SensorDao.activationSensor(sensorid, sensorstatus);
		if(!status){
			request.getSession().setAttribute("error", "Errore nel cambio di stato del sensore\n");
		}else{
			request.getSession().setAttribute("message", "Cambio di stato eseguito con successo\n");
		}
		response.sendRedirect("collaudo");
	}
}
