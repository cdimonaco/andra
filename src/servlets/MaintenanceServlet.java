package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MysqlDAOFactory;
import dao.alarm.AlarmHandler;
import dao.alarmactions.AlarmActions;
import dao.module.ModuleHandler;
import dao.sensor.SensorsHandler;
import models.ActionModel;
import models.AlarmModel;
import models.ModuleModel;
import models.SensorModel;

/**
 * Servlet implementation class MaintenanceServlet
	 * Va fatto menzione dell'utilizzo delle Sessioni, necessarie per il trasferimento dei messaggi flash al frontend, per evitare il sovrapporsi
	 * di messaggi e/o di errori, la sessione viene invalidata dopo la trasmissione dei messsaggi e/o errori.
 */
@WebServlet("/collaudo")
public class MaintenanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MaintenanceServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		SensorsHandler SensorDao = MysqlDAOFactory.getInstance().getSensorsDAO();
		ModuleHandler ModuleDao = MysqlDAOFactory.getInstance().getModuleDAO();
		AlarmHandler AlarmDao = MysqlDAOFactory.getInstance().getAlarmDAO();
		AlarmActions ActionsDao = MysqlDAOFactory.getInstance().getAlarmsActionsDAO();
		ArrayList<ModuleModel> allModules = ModuleDao.getAll();
		ArrayList<SensorModel> allSensors = SensorDao.getAll();
		ArrayList<AlarmModel> allAlarms = AlarmDao.getAll();
		ArrayList<ActionModel> allActions = ActionsDao.getAll();
        request.setAttribute("sensors", allSensors);
        request.setAttribute("modules", allModules);
        request.setAttribute("alarms", allAlarms);
        request.setAttribute("actions", allActions);
        request.getRequestDispatcher("/collaudo.jsp").forward(request, response);
        request.getSession().invalidate();
	}
}
