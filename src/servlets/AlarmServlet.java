package servlets;

import java.io.IOException;
import java.util.Date;

import dao.*;
import dao.alarm.AlarmHandler;
import models.AlarmModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class AlarmServlet
 * 
 * Tramite il method HTTP "POST", indentifico il tipo di azione che deve compiere la servlet, utilizzo un campo nel form
	 * con attributo "hidden", contenente l'azione.
	 * Sarà quindi compito preliminare della servlet, leggere questo parametro dalla richiesta per richiamare il metodo corretto.
	 * Va fatto menzione dell'utilizzo delle Sessioni, necessarie per il trasferimento dei messaggi flash al frontend, per evitare il sovrapporsi
	 * di messaggi e/o di errori, la sessione viene invalidata dopo la trasmissione dei messsaggi e/o errori.
 */
@WebServlet("/alarm")
public class AlarmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlarmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("_method");
		if(method.equalsIgnoreCase("DELETE")){
			doDelete(request,response);
			return;
		}
		String alarmname = request.getParameter("alarmname");
		int sensortoalarm = Integer.parseInt(request.getParameter("alarmsensor"));
		AlarmHandler alarmDao = MysqlDAOFactory.getInstance().getAlarmDAO();
		AlarmModel newAlarm = new AlarmModel(alarmname,new Date(),sensortoalarm,1);
		boolean status = alarmDao.Insert(newAlarm);
		if(!status){
			request.getSession().setAttribute("error", "Errore nella creazione dell'allarme \n");
		}else{
			request.getSession().setAttribute("message", "Creazione allarme eseguita");
		}
		response.sendRedirect("collaudo");
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AlarmHandler alarmDao = MysqlDAOFactory.getInstance().getAlarmDAO();
		boolean status = alarmDao.Delete();
		if(!status){
			request.getSession().setAttribute("error", "Errore nell'interruzione dell'allarme \n");
		}else{
			request.getSession().setAttribute("message", "Stop allarme eseguito");
		}
		response.sendRedirect("sensors");
	}

}
