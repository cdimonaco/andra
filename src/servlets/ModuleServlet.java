package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.MysqlDAOFactory;
import models.ModuleModel;
import dao.module.*;
/**
 * Servlet implementation class ModuleServlet
 * Tramite il method HTTP "POST", indentifico il tipo di azione che deve compiere la servlet, utilizzo un campo nel form
	 * con attributo "hidden", contenente l'azione.
	 * Sarà quindi compito preliminare della servlet, leggere questo parametro dalla richiesta per richiamare il metodo corretto.
	 * Va fatto menzione dell'utilizzo delle Sessioni, necessarie per il trasferimento dei messaggi flash al frontend, per evitare il sovrapporsi
	 * di messaggi e/o di errori, la sessione viene invalidata dopo la trasmissione dei messsaggi e/o errori.
 */
@WebServlet("/modules")
public class ModuleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModuleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getParameter("_method");
		if(method.equalsIgnoreCase("PUT")){
			doPut(request,response);
			return;
		}
		String moduleName = request.getParameter("modulename");
		String moduleDescription = request.getParameter("description");
		ModuleHandler moduleDAO = MysqlDAOFactory.getInstance().getModuleDAO();
		ModuleModel newModule = new ModuleModel(0,moduleName,moduleDescription);
		boolean status = moduleDAO.Insert(newModule);
		if(!status){
			request.getSession().setAttribute("error", "Errore creazione modulo");
		}else{
			request.getSession().setAttribute("message", "Modulo creato con successo!");
		}
		response.sendRedirect("collaudo");
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ModuleHandler moduleDAO = MysqlDAOFactory.getInstance().getModuleDAO();
		int moduleid = Integer.parseInt(request.getParameter("moduleid"));
		int sensorid = Integer.parseInt(request.getParameter("sensorid"));
		boolean status = moduleDAO.Attach(moduleid, sensorid);
		if(!status){
			request.getSession().setAttribute("error", "Errore nell'assegnazione del modulo. Controlla che non sia già stato assegnato al sensore\n");
		}else{
			request.getSession().setAttribute("message", "Modulo assegnato con successo al sensore!");
		}
		response.sendRedirect("collaudo");
	}
}
