package models;

import java.util.Date;

public class AlarmModel {
	private int activated;
	private String Name;
	private int Sensor;
	private Date Timestamp;
	
	public AlarmModel(String name, Date timestamp, int sensor, int activated) {
		super();
		this.Name = name;
		this.Timestamp = timestamp;
		this.Sensor = sensor;
		this.activated = activated;
	}
	public int getActivated() {
		return activated;
	}
	public void setActivated(int activated) {
		this.activated = activated;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getSensor() {
		return Sensor;
	}
	public void setSensor(int sensor) {
		Sensor = sensor;
	}
	public Date getTimestamp() {
		return Timestamp;
	}
	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}
		
}
