package models;

public class ActionModel {
	private String Name;
	private int id;
	private int sensor;
	private int activated;
	
	public ActionModel(String name, int id, int sensor,int activated) {
		super();
		Name = name;
		this.id = id;
		this.sensor = sensor;
		this.activated=activated;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSensor() {
		return sensor;
	}
	public void setSensor(int sensor) {
		this.sensor = sensor;
	}
	public int getActivated() {
		return activated;
	}
	public void setActivated(int activated) {
		this.activated = activated;
	}
	
	
}
