package models;

public class SensorModel{
	
	private int Id;
	private String Name;
	private float maxValue;
	private float minValue;
	private float currValue;
	private int activated;
	

	public SensorModel(int id, String name, float maxValue, float minValue, float currValue,int activated) {
		super();
		Id = id;
		Name = name;
		this.maxValue = maxValue;
		this.minValue = minValue;
		this.currValue = currValue;
		this.activated=activated;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public float getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}

	public float getMinValue() {
		return minValue;
	}

	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	public float getCurrValue() {
		return currValue;
	}

	public void setCurrValue(float currValue) {
		this.currValue = currValue;
	}

	public int getActivated() {
		return activated;
	}

	public void setActivated(int activated) {
		this.activated = activated;
	}
	
	
}
